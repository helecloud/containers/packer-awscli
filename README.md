# Packer and AWSCLI

[HashiCorp Packer](https://www.packer.io/) and [AWS-CLI (v2)](https://aws.amazon.com/cli/) for using inside CI or local user machines.

https://gitlab.com/helecloud/containers/packer-awscli

## Usage

I would expect that this is mainly used inside CI however, you can use it locally if you want.

### Run locally

```
$~   docker run --rm -it --entrypoint /bin/bash registry.gitlab.com/helecloud/containers/packer-awscli:latest 
```

### Run inside GitLab CI

```yaml
aws-cli:
  stage: aws
  image: registry.gitlab.com/helecloud/containers/packer-awscli:latest
  script:
    - packer ... # enter packer commands
    - aws ... # enter your aws command
```

## Maintainer

* Will Hall